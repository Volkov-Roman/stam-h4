/**
 * Padding hex component if necessary. 
 * Hex component representation requires
 * two characters.
 * @param {string} comp 
 * @returns {string} two hexadecomal characters
 */
const pad = (comp) => {
    const padded = comp.length == 2 ? comp: "0" + comp;
    return padded;
};

/**
 * RGB-to-HEX conversion
 * @param {number} r RED 0-255
 * @param {number} g GREEN 0-255
 * @param {number} b BLUE 0-255
 * @returns {string} in hex color format
 */
export const rgb_to_hex = (r, g, b) => {
    const HEX_RED = r.toString(16);
    const HEX_GREEN = g.toString(16);
    const HEX_BLUE = b.toString(16);
    return "#" + pad(HEX_RED) + pad(HEX_GREEN) + pad(HEX_BLUE);
};

/**
 * HEX-to-RGB conversion
 * @param {string} hex in hex color format without '#'!
 * @returns {string} for example "rgb(255,0,0)"
 */
export const hex_to_rgb = (hex) => {
    const red = parseInt(hex.substring(0, 2), 16);
    const green = parseInt(hex.substring(2, 4), 16);
    const blue = parseInt(hex.substring(4, 6), 16);
    return `rgb(${red},${green},${blue})`;
};