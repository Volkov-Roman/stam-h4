import { describe, it } from "mocha";
import { expect } from "chai";
import { rgb_to_hex, hex_to_rgb } from "../src/converter.js";

describe("RGB-to-HEX Converter", () => {
    it("is a function", () => {
        expect(rgb_to_hex).to.be.a('function');
    });
    it("should return a string", () => {
        expect(rgb_to_hex(0,0,0)).to.be.a('string');
    });
    it("first character is hashtag", () => {
        expect(rgb_to_hex(0,0,0)[0]).to.equal('#');
    });
    it("should convert RED value correctly", () => {
        expect(rgb_to_hex(0,0,0).substring(0, 3)).to.equal('#00');
        expect(rgb_to_hex(255,0,0).substring(0, 3)).to.equal('#ff');
        expect(rgb_to_hex(136,0,0).substring(0, 3)).to.equal('#88');
        expect(rgb_to_hex(136,0,0).substring(0, 3)).to.not.equal('#00');
    });
    it("should convert GREEN value correctly", () => {
        expect(rgb_to_hex(0,0,0).substring(0, 3)).to.equal('#00');
        expect(rgb_to_hex(0,255,0).substring(3, 5)).to.equal('ff');
        expect(rgb_to_hex(0,136,0).substring(3, 5)).to.equal('88');
        expect(rgb_to_hex(0,136,0).substring(3, 5)).to.not.equal('00');
    });
    it("should convert BLUE value correctly", () => {
        expect(rgb_to_hex(0,0,0).substring(0, 3)).to.equal('#00');
        expect(rgb_to_hex(0,0,255).substring(5, 7)).to.equal('ff');
        expect(rgb_to_hex(0,0,136).substring(5, 7)).to.equal('88');
        expect(rgb_to_hex(0,0,136).substring(5, 7)).to.not.equal('00');
    });
    it("should convert RGB to HEX correctly", () => {
        expect(rgb_to_hex(0,0,0)).to.equal('#000000');
        expect(rgb_to_hex(255,255,255)).to.equal('#ffffff');
        expect(rgb_to_hex(255,136,0)).to.equal('#ff8800');
        expect(rgb_to_hex(255,236,0)).to.not.equal('#00ff00');
    });
});

describe("HEX-to-RGB Converter", () => {
    it("is a function", () => {
        expect(hex_to_rgb).to.be.a('function');
    });
    it("should return a string", () => {
        expect(hex_to_rgb("000000")).to.be.a('string');
    });
    it("starts with 'rgb('", () => {
        expect(hex_to_rgb("000000").substring(0, 4)).to.equal('rgb(');
    });
    it("ends with ')'", () => {
        expect(hex_to_rgb("000000").slice(-1)).to.equal(')');
    });
    it("should convert RED value correctly", () => {
        expect(hex_to_rgb("000000").substring(4, 6)).to.equal('0,');
        expect(hex_to_rgb("ff0000").substring(4, 8)).to.equal('255,');
        expect(hex_to_rgb("880000").substring(4, 8)).to.equal('136,');
        expect(hex_to_rgb("880000").substring(4, 8)).to.not.equal('135,');
    });
    it("should convert GREEN value correctly", () => {
        expect(hex_to_rgb("000000").substring(6, 8)).to.equal('0,');
        expect(hex_to_rgb("00ff00").substring(6, 10)).to.equal('255,');
        expect(hex_to_rgb("008800").substring(6, 10)).to.equal('136,');
        expect(hex_to_rgb("008800").substring(6, 10)).to.not.equal('135,');
    });
    it("should convert BLUE value correctly", () => {
        expect(hex_to_rgb("000000")[8]).to.equal('0');
        expect(hex_to_rgb("0000ff").substring(8, 11)).to.equal('255');
        expect(hex_to_rgb("000088").substring(8, 11)).to.equal('136');
        expect(hex_to_rgb("000088").substring(8, 11)).to.not.equal('135');
    });
    it("should convert HEX to RGB correctly", () => {
        expect(hex_to_rgb("000000")).to.equal('rgb(0,0,0)');
        expect(hex_to_rgb("0088ff")).to.equal('rgb(0,136,255)');
        expect(hex_to_rgb("008800")).to.equal('rgb(0,136,0)');
        expect(hex_to_rgb("880000")).to.not.equal('rgb(0,0,0)');
    });
});
